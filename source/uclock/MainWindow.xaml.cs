﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Drawing;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using uclock.ClockClass;
using uclock.View;
using uclock;
using Application = System.Windows.Application;
using ContextMenu = System.Windows.Forms.ContextMenu;
using MenuItem = System.Windows.Forms.MenuItem;
using MessageBox = System.Windows.MessageBox;
using MouseEventArgs = System.Windows.Forms.MouseEventArgs;
using MouseEventHandler = System.Windows.Forms.MouseEventHandler;
using UserControl = System.Windows.Controls.UserControl;

namespace uclock
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly MainViewModel _mainViewModel;
        private NotifyIcon _notifyIcon;

        public MainWindow()
        {
            _mainViewModel = ViewModelLocator.MainStatic;
            _mainViewModel.MusicPlayer = new RelayCommand(MusicPlayer);
            InitializeComponent();
        }

        #region 托盘


        private void NotifyIcon()
        {
            this.Visibility = Visibility.Hidden;
            this._notifyIcon = new NotifyIcon
                {
                    BalloonTipText = @"Luminous Staring！",
                    Text = @"Luminous！",
                    Visible = true,
                };


            var streamResourceInfo =
                Application.GetResourceStream(
                    new Uri("pack://application:,,,/uclock;component/Resource/Image/ClockiConk.ico"));
            if (streamResourceInfo != null)
            {
                Stream iconStream = streamResourceInfo.Stream;
                _notifyIcon.Icon = new System.Drawing.Icon(iconStream);
            }


            this._notifyIcon.ShowBalloonTip(1000);
            this._notifyIcon.MouseClick += new MouseEventHandler(notifyIcon_MouseClick);
            var item1 = new MenuItem("Main");
            item1.Click += new EventHandler(item1_Click);
            var light = new MenuItem("Light");
            light.Click += new EventHandler(Light_Click);
            var dark = new MenuItem("Dark");
            dark.Click += new EventHandler(Dark_Click);
            var stopmucis = new MenuItem("StopMusic");
            stopmucis.Click += new EventHandler(StopMusi_Click);
            var item2 = new MenuItem("Exit");
            item2.Click += new EventHandler(item2_Click);
            var menuItems = new MenuItem[] {item1, light, dark, stopmucis, item2};
            this._notifyIcon.ContextMenu = new ContextMenu(menuItems);
            this.StateChanged += new EventHandler(MainWindow_StateChanged);
            this.Closing += new System.ComponentModel.CancelEventHandler(MainWindow_Closing);
        }


        private void notifyIcon_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (this.Visibility == Visibility.Visible)
                {
                    this.Visibility = Visibility.Hidden;
                }
                else
                {
                    this.Visibility = Visibility.Visible;
                    this.Activate();
                }
            }
        }


        private QuartzClock _light;
        private void Light_Click(object sender, EventArgs e)
        {
            if (null != _dark)
            {
                _dark.Close();
                _dark = null;
            }
            if (null == _light)
            {
                this.Visibility = Visibility.Collapsed;
                _light = new QuartzClock();
                _light.Show();
                _light.Activate();
            }
            else
            {
                _light.Show();
                _light.Activate();
            }
         
        }

        private ElectronicCock _dark;
        private void Dark_Click(object sender, EventArgs e)
        {
            if (null != _light)
            {
                _light.Close();
                _light = null;
            }
            if (null == _dark)
            {
                this.Visibility = Visibility.Collapsed;
                _dark = new ElectronicCock();
                _dark.Show();
                _dark.Activate();
            }
            else
            {
                _dark.Show();
                _dark.Activate();
            }
        }

        private void item1_Click(object sender, EventArgs e)
        {
            if (null != _dark)
            {
                _dark.Close();
                _dark = null;
            }
            if (null != _light)
            {
                _light.Close();
                _light = null;
            }
            this.Visibility = Visibility.Visible;
            this.Activate();
        }

        private void item2_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void StopMusi_Click(object sender, EventArgs e)
        {
            BackgroudMusic.Close();
        }

        private void MainWindow_StateChanged(object sender, EventArgs e)
        {
            if (this.WindowState == WindowState.Minimized)
            {
                this.Visibility = Visibility.Hidden;
            }
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (System.Windows.MessageBox.Show("Do you want exit Luminous？",
                                               "Luminous",
                                               MessageBoxButton.YesNo,
                                               MessageBoxImage.Question,
                                               MessageBoxResult.No) == MessageBoxResult.Yes)
            {
                this._notifyIcon.Visible = false;
                Environment.Exit(0);
            }
            else
            {
                e.Cancel = true;
            }
        }

        #endregion


        //SetClock
        private void ClockRecent_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count != 0)
            {
                OutPutWindow(new ClockAdd()
                    {
                        Width = 635,
                        Height = 418
                    });
            }
        }


        //MusicOpen
        private void UIElement_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            OutPutWindow(new MusicPlayer()
                {
                    Width = 635,
                    Height = 418
                });
        }


        private void OpenSetClock(object sender, MouseButtonEventArgs e)
        {
            OutPutWindow(new ClockAdd()
                {
                    Width = 635,
                    Height = 418
                });
        }


        private void OutPutWindow(UserControl userControl)
        {
            MainPanle.Visibility = Visibility.Collapsed;
            Contenpanl.Visibility = Visibility.Visible;
            Contenpanl.Children.Clear();
            Contenpanl.Children.Add(userControl);
        }

        private void Close(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void TipPan(object sender, RoutedEventArgs e)
        {
            NotifyIcon();
        }

        private void Minset(object sender, RoutedEventArgs e)
        {
           this.WindowState=WindowState.Minimized;
        }

        private void DragMive(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void MusicPlayer()
        {
            this.Dispatcher.Invoke(new Action(() =>
                {
                    BackgroudMusic.Source = new Uri(AppDomain.CurrentDomain.BaseDirectory + "Resource/Music/123.mp3",
                                                    UriKind.Absolute);
                    BackgroudMusic.Play();
                }));
        }
    }
}
