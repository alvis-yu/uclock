﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using uclock.ClockClass;

namespace uclock.Model
{
    public class Respostory
    {

        public static void InsertMusic(Musics musics)
        {
            SqLiteWrapperHelper.ExnoculSql(
                String.Format("insert into Musics (Id,Name,Path,Filelength,Playtime) values('{0}','{1}','{2}','{3}','{4}')",
                              musics.Id, musics.Name, musics.Path, musics.Filelength, musics.Playtime));
        }

        public static IList<Musics> GetAllMusic()
        {
            var dt = SqLiteWrapperHelper.ExculSql(String.Format("select * from Musics"));
            IList<Musics> musicses = new List<Musics>(dt.Rows.Count);
            foreach (DataRow row in dt.Rows)
            {
                musicses.Add(new Musics
                    {
                        Id = row["Id"].ToString(),
                        Name = row["Name"].ToString(),
                        Path = row["Path"].ToString(),
                        Filelength = row["Filelength"].ToString(),
                        Playtime = row["Playtime"].ToString(),
                    });
            }
            return musicses;
        }



        public static void InsertRecore(ClockRecord clockRecord)
        {
            SqLiteWrapperHelper.ExnoculSql(
                String.Format(
                    "insert into ClockRecord (Id,RecordConent,ClockDate,Music,MusicName,Beer,Email,EmailAdress) values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')",
                    clockRecord.Id, clockRecord.RecordConent, clockRecord.ClockDate, clockRecord.Music.BoolCoverString(),
                    clockRecord.MusicName, clockRecord.Beer.BoolCoverString(), clockRecord.Email.BoolCoverString(),
                    clockRecord.EmailAdress));
        }


        public static void DeleteRecore(ClockRecord clockRecord)
        {
            if (null != clockRecord)
            {
                SqLiteWrapperHelper.ExnoculSql(
               String.Format(
                   "delete ClockRecord where Id='{0}'", clockRecord.Id));
            }
        }

        public static IList<ClockRecord> GetAllClockRecord()
        {
            var dt = SqLiteWrapperHelper.ExculSql(String.Format("select * from ClockRecord  order by  RecordConent"));
            IList<ClockRecord> clockRecords = new List<ClockRecord>(dt.Rows.Count);
            foreach (DataRow row in dt.Rows)
            {
                clockRecords.Add(new ClockRecord
                    {
                        Id = row["Id"].ToString(),
                        RecordConent = row["RecordConent"].ToString(),
                        ClockDate = row["ClockDate"].ToString(),
                        Music = row["Music"].ToString().StringCoverBool(),
                        MusicName = row["MusicName"].ToString(),
                        Beer = row["Beer"].ToString().StringCoverBool(),
                        Email = row["Email"].ToString().StringCoverBool(),
                        EmailAdress = row["EmailAdress"].ToString()
                    });
            }
            return clockRecords;
        }

    }
}
