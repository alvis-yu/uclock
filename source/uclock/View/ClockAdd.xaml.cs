﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using uclock.ClockClass;
using uclock.Model;
using uclock;
using uclock.ViewModel;

namespace uclock.View
{
    /// <summary>
    /// ClockAdd.xaml 的交互逻辑
    /// </summary>
    public partial class ClockAdd : UserControl
    {

        private readonly ClockAddViewModel _clockAddViewModel;

        public ClockAdd()
        {
            InitializeComponent();
            _clockAddViewModel = ViewModelLocator.ClockAddStatic;

        }


        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(Datetime.Text))
            {
                MessageBox.Show("请选择时间!");
                return;
            }

            var clockre = new ClockRecord
                {
                    Id = Guid.NewGuid().ToString(),
                    Beer = Beer.IsChecked ?? false,
                    EmailAdress = EmailAdress.Text,
                    ClockDate =
                        DateTime.Parse(Datetime.Text.ToString(CultureInfo.InvariantCulture))
                                .ToString("yyyy-MM-dd HH:mm:ss"),
                    Email = Emailtip.IsChecked ?? false,
                    Music = Musitip.IsChecked ?? false,
                    RecordConent = Record.Text
                };
            _clockAddViewModel.CurrentDateTimeClock = clockre;
            _clockAddViewModel.AddRecord.Execute(null);

        }


        private void Returenmaiin(object sender, RoutedEventArgs e)
        {
            _clockAddViewModel.CurrentDateTimeClock = null;
            UiHelp.Returmain(this);
        }
    }

}
