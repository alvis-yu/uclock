﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using uclock.ClockClass;
using uclock.Model;
using uclock;
using Button = System.Windows.Controls.Button;
using UserControl = System.Windows.Controls.UserControl;
using System.IO;

namespace uclock.View
{
    /// <summary>
    /// MusicPlayer.xaml 的交互逻辑
    /// </summary>
    public partial class MusicPlayer : UserControl
    {

        private TimeSpan _position;
        private readonly DispatcherTimer _timer = new DispatcherTimer();


        private readonly MusicPlayerViewModel _musicPlayerView;
        private readonly Style _btnStar;
        private readonly Style _btnStop;

        public MusicPlayer()
        {
            InitializeComponent();
            _musicPlayerView = ViewModelLocator.MusicPlayerStat;

            _btnStar = FindResource("BtnStar") as Style;
            _btnStop = FindResource("BtnStop") as Style;
            _timer.Interval = TimeSpan.FromMilliseconds(1000);
            _timer.Tick += new EventHandler(Ticktock);
            _timer.Start();

        }

        private void Ticktock(object sender, EventArgs e)
        {
            _musicPlayerView.SliderCurrent = Media.Position.TotalSeconds;
        }


        private void Play_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            if (btn != null && _btnStar == btn.Style)
            {
                Media.Play();
                btn.Style = _btnStop;
            }
            else if (btn != null && _btnStop == btn.Style)
            {
                Media.Pause();
                btn.Style = _btnStar;
            }

        }

        private void media_MediaOpened(object sender, RoutedEventArgs e)
        {
            _position = Media.NaturalDuration.TimeSpan;
            _musicPlayerView.SliderMax = _position.TotalSeconds;
            _musicPlayerView.SelectMusic.Playtime = ((int) _position.TotalSeconds/60).ToString("00") + ":" +
                                                    ((int)_position.TotalSeconds % 60).ToString("00");
            _musicPlayerView.SelectMusic = _musicPlayerView.SelectMusic;
            _musicPlayerView.InsertMusic.Execute(null);
        }

        private void MusicSlider_OnDragStarted(object sender, DragStartedEventArgs e)
        {
            Media.Stop();
        }

        private void MusicSlider_OnDragCompleted(object sender, DragCompletedEventArgs e)
        {
            var pos = Convert.ToInt32(MusicSlider.Value);
            Media.Position = new TimeSpan(0, 0, 0, pos, 0);
            Media.Play();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var of = new OpenFileDialog();
            if (of.ShowDialog() == DialogResult.OK)
            {
                var fileInfo = new FileInfo(of.FileName);
                var musics = new Musics()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Name = fileInfo.Name,
                        Path = fileInfo.FullName,
                        Filelength = FormatFileSize(fileInfo.Length) 
                    };
                _musicPlayerView.SelectMusic = musics;
                Media.Play();
                Btnstar.Style = _btnStop;
            }
        }

        public  String FormatFileSize(Int64 fileSize)
        {
            if (fileSize < 0)
            {
                throw new ArgumentOutOfRangeException("fileSize");
            }
            else if (fileSize >= 1024 * 1024 * 1024)
            {
                return string.Format("{0:########0.00} GB", ((Double)fileSize) / (1024 * 1024 * 1024));
            }
            else if (fileSize >= 1024 * 1024)
            {
                return string.Format("{0:####0.00} MB", ((Double)fileSize) / (1024 * 1024));
            }
            else if (fileSize >= 1024)
            {
                return string.Format("{0:####0.00} KB", ((Double)fileSize) / 1024);
            }
            else
            {
                return string.Format("{0} bytes", fileSize);
            }
        }

        private void ReturnMain(object sender, RoutedEventArgs e)
        {
            _timer.Stop();
            Media.Stop();
            UiHelp.Returmain(this);
        }
    }
}
