﻿using System;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using uclock.ClockClass;

namespace uclock.View
{
    /// <summary>
    /// QuartzClock.xaml 的交互逻辑
    /// </summary>
    public partial class QuartzClock : Window
    {
        private double _secondangle = 270;
        private readonly Action _action;
        public QuartzClock()
        {
            _action = new Action(UpdateSecond);

            InitializeComponent();
            var aTimer = new Timer()
            {
                Interval = 50,
                Enabled = true,
            };
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
        }




        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            Dispatcher.Invoke(_action);
        }

        private void UpdateSecond()
        {
            var dateTime = DateTime.Now;
            Hour.Angle = _secondangle + 720/24*dateTime.Hour;
            Mini.Angle = _secondangle + 6*dateTime.Minute;
            Second.Angle = _secondangle + (6.0 / 1000 )* (dateTime.Millisecond + dateTime.Second * 1000);
        }

        private void UIElement_OnMouseEnter(object sender, MouseEventArgs e)
        {
            UiHelp.DoMove(GridBackground, Brush.OpacityProperty, 0.8, 0, 0, 0.3);
            UiHelp.DoMove(Anger, UIElement.OpacityProperty, 0.7, 0, 0, 0.3);
            
        }

        private void UIElement_OnMouseLeave(object sender, MouseEventArgs e)
        {
            UiHelp.DoMove(GridBackground, Brush.OpacityProperty, 0.6, 0, 0, 0.3);
            UiHelp.DoMove(Anger, UIElement.OpacityProperty, 1, 0, 0, 0.3);
        }


        private void DragMive(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }
    }
}
