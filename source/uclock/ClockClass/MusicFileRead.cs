﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace uclock.ClockClass
{
    internal class MusicFileRead
    {
        public static string[] GetAudioFileInfo(string path)
        {
            path = Uri.UnescapeDataString(path);

            byte[] b = new byte[128];
            string[] infos = new string[5]; //Title; Singer; Album; Year; Comm;
            bool isSet = false;

            //Read bytes
            try
            {
                FileStream fs = new FileStream(path, FileMode.Open);
                fs.Seek(-128, SeekOrigin.End);
                fs.Read(b, 0, 128);
                //Set flag
                String sFlag = System.Text.Encoding.Default.GetString(b, 0, 3);
                sFlag = sFlag.Replace("\0", "");
                if (sFlag.CompareTo("TAG") == 0) isSet = true;

                if (isSet)
                {
                    infos[0] = System.Text.Encoding.Default.GetString(b, 3, 30); //Title
                    infos[1] = System.Text.Encoding.Default.GetString(b, 33, 30); //Singer
                    infos[2] = System.Text.Encoding.Default.GetString(b, 63, 30); //Album
                    infos[3] = System.Text.Encoding.Default.GetString(b, 93, 4); //Year
                    infos[4] = System.Text.Encoding.Default.GetString(b, 97, 30); //Comm
                }
                fs.Close();
                fs.Dispose();
            }
            catch (IOException ex)
            {

            }

            return infos;
        }
    }
}
