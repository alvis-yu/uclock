﻿using System;
using System.Data;
using SQLiteWrapper;

namespace uclock.ClockClass
{
    public static class SqLiteWrapperHelper
    {
        private static readonly SQLiteBase SqliteBase = new SQLiteBase();
        public static String Constr = AppDomain.CurrentDomain.BaseDirectory + "Resource\\ClockDataBase.sqlite";

        public static DataTable ExculSql(String sqlstr)
        {
            try
            {
                SqliteBase.OpenDatabase(Constr);
                DataTable dt = SqliteBase.ExecuteQuery(sqlstr);
                return dt;
            }
            catch
            {
                SqliteBase.CloseDatabase();
                return null;
            }
            finally
            {
                SqliteBase.CloseDatabase();
            }
        }


        public static void ExnoculSql(String sqlstr)
        {
            try
            {
                SqliteBase.OpenDatabase(Constr);
                SqliteBase.ExecuteNonQuery(sqlstr);
            }
            catch
            {
                SqliteBase.CloseDatabase();
            }
            finally
            {
                SqliteBase.CloseDatabase();
            }
        }
    }
}
