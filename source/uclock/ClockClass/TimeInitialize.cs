﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using uclock.Model;
using uclock;

namespace uclock.ClockClass
{
    public abstract class TimeInitialize
    {
        protected abstract DateTime GetNowTime();

        public void Save()
        {
            var userConfig = new UserConfig();
            var timeSpan = DateTime.Now - GetNowTime();
            userConfig.TimeSpan = timeSpan.ToString();
            File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + "config.cong",
                              CommXmlSerialize.ObjectSerializeXml(userConfig));

        }
    }

    internal class WebTime : TimeInitialize
    {
        protected override DateTime GetNowTime()
        {
            DateTime dt;
            WebRequest wrt = null;
            WebResponse wrp = null;
            try
            {
                wrt = WebRequest.Create("http://www.beijing-time.org/time.asp");
                wrp = wrt.GetResponse();

                string html;
                using (var stream = wrp.GetResponseStream())
                {
                    using (var sr = new StreamReader(stream, Encoding.UTF8))
                    {
                        html = sr.ReadToEnd();
                    }
                }

                string[] tempArray = html.Split(';');
                for (var i = 0; i < tempArray.Length; i++)
                {
                    tempArray[i] = tempArray[i].Replace("\r\n", "");
                }

                string year = tempArray[1].Split('=')[1];
                string month = tempArray[2].Split('=')[1];
                string day = tempArray[3].Split('=')[1];
                string hour = tempArray[5].Split('=')[1];
                string minite = tempArray[6].Split('=')[1];
                string second = tempArray[7].Split('=')[1];

                dt = DateTime.Parse(year + "-" + month + "-" + day + " " + hour + ":" + minite + ":" + second);
            }
            catch (WebException)
            {
                return DateTime.Parse("2011-1-1");
            }
            catch (Exception)
            {
                return DateTime.Parse("2011-1-1");
            }
            finally
            {
                if (wrp != null)
                    wrp.Close();
                if (wrt != null)
                    wrt.Abort();
            }

           dt= dt.Add(new TimeSpan(0, 0, 0, 10));
            return dt;
        }
    }

    internal class LocalTime : TimeInitialize
    {
        protected override DateTime GetNowTime()
        {
            return DateTime.Now;
        }
    }

    public class SetLocalTime : TimeInitialize
    {
        protected override DateTime GetNowTime()
        {
           /* return ViewModelLocator.MainStatic.SetCurrentTime;*/
            return DateTime.Now;
        }
    }

    public enum TimeType
    {
        WebTime = 0,
        LocalTime = 1,
        SetTime = 2
    }

    public class TimeFactory
    {
        public static TimeInitialize GetTime(TimeType timeType)
        {

            TimeInitialize timeInitialize = null;
            switch (timeType)
            {
                case TimeType.WebTime:
                    timeInitialize = new WebTime();
                    break;
                case TimeType.LocalTime:
                    timeInitialize = new LocalTime();
                    break;
                case TimeType.SetTime:
                    timeInitialize = new SetLocalTime();
                    break;
            }

            return timeInitialize;
        }
    }


}