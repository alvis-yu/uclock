﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace uclock.ClockClass
{
    class UiHelp
    {
        //动画
        public static void DoMove(Animatable uie, DependencyProperty dp, double to, double ar, double dr, double duration)
        {
            var doubleAnimation = new DoubleAnimation
                {
                    To = to,
                    Duration = TimeSpan.FromSeconds(duration),
                    AccelerationRatio = ar,
                    DecelerationRatio = dr,
                    FillBehavior = FillBehavior.HoldEnd
                };//创建双精度动画对象
            uie.BeginAnimation(dp, doubleAnimation);//设置动画应用的属性并启动动画
        }

        public static void DoMove(UIElement uie, DependencyProperty dp, double to, double ar, double dr, double duration)
        {
            var doubleAnimation = new DoubleAnimation
                {
                    To = to,
                    Duration = TimeSpan.FromSeconds(duration),
                    AccelerationRatio = ar,
                    DecelerationRatio = dr,
                    FillBehavior = FillBehavior.HoldEnd
                };//创建双精度动画对象
            uie.BeginAnimation(dp, doubleAnimation);//设置动画应用的属性并启动动画
        }


        public static void Returmain(UIElement uiElement)
        {
            var window = Window.GetWindow(uiElement);
            var main = window.FindName("MainPanle") as Grid;
            main.Visibility = Visibility.Visible;
            var contenpanl = window.FindName("Contenpanl") as Grid;
            contenpanl.Children.Clear(); 
            contenpanl.Visibility = Visibility.Collapsed;
          
        }

    }
}
